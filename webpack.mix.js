const mix = require("laravel-mix");

mix
  .js("src/js/app.js", "js")
  .postCss("src/css/app.css", "css", [
    // listar todos los plugins para PostCSS necesarios...
    // require("postcss-import"),
    // require("postcss-custom-properties"),
    // o dejar este Array vacío si no se va a emplear plugin alguno
  ])
  //.sass("src/sass/app.scss", "css")
  .setPublicPath("public");

// Disable mix-manifest.json
Mix.manifest.refresh = (_) => void 0;
