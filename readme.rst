########################################
Proyecto Práctico con CodeIgniter 3.1.11
########################################

El propósito del proyecto es efectuar una práctica para el manejo del framework de `CodeIgniter <https://codeigniter.com>`_, el uso de `Boostrap 4.6 <https://getbootstrap.com/docs/4.6/getting-started/introduction>`_, `jQuery <https://jquery.com>`_ y la interacción con alguna otra librería JS como `DataTables <https://datatables.net>`_ y `Chart.js <https://www.chartjs.org>`_.


*******************
Detalles Generales
*******************

Tras establecer el proyecto en el servidor local, se crea la base de datos para guardar la información relativa a la aplicación respecto al modelo de "Empleados" (tabla "employees") y al modelo de Puestos (tabla "positions").

Para gestionar la base de datos, se crea un usuario administrador de la misma con todos los privilegios sobre ella.

Si la base de datos se llama, por ejemplo "database_proy", el usuario tiene como nombre de usuario "db_user_admin" y su contraseña fuera "", el siguiente comando SQL, además de crear la cuenta de usuario en el servidor de bases de datos, le otorgaría plenos privilegios sobre la base de datos en cuestión:

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, CREATE VIEW, EVENT, TRIGGER, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EXECUTE ON `database_proy`.* TO 'db_user_admin'@'%' IDENTIFIED BY 'XXXX';

(cambiar los parámetros de nombre de base de datos, usuario y contraseña por los adecuados en cada caso particular)


******************************
Pasos para el Uso del Proyecto
******************************

Necesario tener instalado `Composer <https://getcomposer.org>`_ y `NodeJS <https://nodejs.org>`_ con NPM para gestionar las dependencias instaladas en el proyecto.

Una vez instalados estos dos gestores de paquetes, se podrán instalar todas las dependencias necesarias mediante estos dos comandos, apuntando el cursor a la carpeta raíz del proyecto:

    |> npm install

    |> composer install

Tras esto, habrán aparecido sendas carpetas dentro del proyecto:

    -> "node_modules", carpeta de las dependencias de JS.

    -> "vendor", dónde se almacenan los paquetes de PHP.

Crear la base de datos para el aplicativo, las tablas pertinentes y aplicar los datos de conexión propios del servidor de bases de datos empleado dentro del archivo de configuración del framework.

Será recomendable, igualmente, disponer de un VirtualHost configurado para que la navegación se adecúe mejor a las rutas configuradas o, al menos, arrancar con el servidor local ofrecido por PHP gracias a ejecutar el siguiente comando, apuntando hacia la carpeta "./public", la carpeta de entrada a la aplicación:

php -S localhost:8040 -t public/


**************************************************
Restructuración de Carpetas y Archivos Principales
**************************************************

Por cuestiones de seguridad, es recomendable mantener el framework y la aplicación fuera de la carpeta establecida como DocumentRoot.
Siguiendo este criterio, se podría establecer esta estructura de carpetas:

|-> application
|
|
|-> public
|       (carpeta en la que alojar archivos estáticos como los de CSS,
|        JS, imágenes, ...)
|
|-> system

Dentro de esta carpeta de publicación "public" (o cualquier nombre que se elija), se alojará todo archivo estático a publicar a los que los navegadores tengan que acceder, archivos tales como: CSS, JS, imágenes, ...
Igualmente, se establecerá, en esta carpeta, el archivo punto de entrada a la aplicación, es decir, el "index.php" que, hasta ahora, se encuentra en la raíz del proyecto.

En la raíz del proyecto, se dispone de un primer archivo ".htaccess" con este código para que una primera redirección tenga como objetivo la carpeta de publicación:

RewriteEngine on
RewriteRule  ^$ public/    [L]
RewriteRule  (.*) public/$1 [L]

Igualmente, se pasa el archivo de entrada "./index.php" dentro de la carpeta "./public".
Y, además, se establece un segundo archivo de ".htaccess" con el que recibir todas las peticiones mediante el reubicado archivo de entrada "./public/index.php":

RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule . index.php [L]

Para que estos cambios de ubicación de archivos lleguen a funcionar, habrá que modificar ciertos parámetros de configuración de variables dentro del, ahora, "./public/index.php":

$system_path = '../system';
$application_folder = '../application';

Ahora, para acceder a la raíz pública del sitio, habrá que añadir la referencia del "/public", osea, una URL local de este tipo:

    http://localhost:8080/amie.consulting-salarios-test/public/

        NOTA EXTRA:
        Si el puerto asignado al servidor local es el puerto por defecto, es decir, el 80, se podrá obviar la referencia de ":8080" dentro de la URL.
        Si fuera otro puerto, sustituir el "8080" por el puerto que fuera.

Si se desea, se podrá evitar esta referencia de LOCALHOST si se configura un VirtualHost que apunte a esta carpeta "./public".


*******************
Creando VirtualHost
*******************

Como ya ha quedado dicho, en este momento, teniendo alojado el proyecto en un servidor local, se accede al mismo a través de un LOCALHOST.
Para disponer de una URL más amigable y similar a lo que será cuando la aplicación se encuentre en un servidor de producción, dentro del servidor local, en este caso, un servidor Apache, se configura un VirtualHost para este proyecto, gracias al siguiente archivo de configuración:


    [ nombre_archivo.conf ]


define ROOT "URL_LOCAL_HACIA_EL_PROYECTO/NOMBRE_PROYECTO/public/"
define SITE "url-virtual-del-proyecto.test"

<VirtualHost *:8080>
    DocumentRoot "${ROOT}"
    ServerName ${SITE}
    ServerAlias *.${SITE}
    <Directory "${ROOT}">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:8443>
    DocumentRoot "${ROOT}"
    ServerName ${SITE}
    ServerAlias *.${SITE}
    <Directory "${ROOT}">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>

Para que la nueva URL tenga efecto, deberá ser registrada, igualmente, en el archivo HOST de la máquina dónde esté alojado el proyecto localmente, mediante la siguiente línea:

127.0.0.1      url-virtual-del-proyecto.test www.url-virtual-del-proyecto.test


Tras todo esto, bastará con reiniciar el servidor para, ya, poder acceder al sitio a través de esta URL:

    http://url-virtual-del-proyecto.test:8080/

        NOTA EXTRA:
        Tener en cuenta lo que ya se comentó en un apartado anterior sobre el tema del puerto del servidor.


********
Composer
********

Mediante Composer, siempre que sea necesario, se pueden instalar dependencias de PHP de terceros dentro del proyecto para añadir funcionalidad variada al proyecto.

    NOTA EXTRA:
    Igualmente, dentro de este tipo de proyectos, o hasta esta versión, también, existe la posibilidad de registrar las librerías de terceros de manera manual, a través de su capeta "third_party".

En un proyecto de CodeIgniter 3.1.11, el archivo "composer.json" que, entre otras cosas, guarda un historial de las dependencias instaladas, reside en la raíz del proyecto.
En este sentido, si se ejecuta un

    |> composer install

las dependencias instaladas se guardarán en una carpeta "vendor" ubicada, igualmente, en la raíz del proyecto, al mismo nivel que, por ejemplo, la carpeta "application".


    ¡¡ATENCIÓN!!
    En el primer intento de instalación de las dependencias inicialmente marcadas, al inicio del proceso, se indica un error en el nombre una de ellas teniendo que corregirle el nombre tal y como indica el mensaje de Error, de "mikey179/vfsStream" a "mikey179/vfsstream".


No obstante, la autocarga de Composer y, por tanto, de las librerías instaladas mediante él, estaría deshabilitada de manera predeterminada en CodeIgniter.
Y sería necesario habilitar esta característica, incluso, antes de instalar cualquier dependencia por este método de Composer.

Para habilitarla, habría dos alternativas, las dos requieren de la modificación de una de las variables del archivo de configuración del framework:

- El archivo a retocar es el de "./application/config/config.php" y la propiedad es la de " $config['composer_autoload'] ".

- Las dos alternativas posibles para habilitar la autocarga de Composer son:

    -> [1d2]

        >> dejar el "composer.json" en la raíz del proyecto e indicar la ruta en la que se encuentra "vendor/autoload.php" con respecto al archivo "./application/config/config.php".

            (En este proyecto, se APLICA ESTA OPCIÓN de la siguiente manera)

Creando, dentro de "./public/index.php", una nueva constante "$vendor_path", al mismo tiempo que se verifica la fiabilidad de la ruta establecida y se define la constante VENDORPATH.
(ver bloques relativos a VENDORPATH en el archivo mencionado)

Gracias a todo ello, se establece la ruta al "../vendor/autoload.php" de esta manera:

$config['composer_autoload'] = VENDORPATH . "autoload.php";


    -> [2d2]

        >> pasar el "composer.json" en la raíz de la carpeta "./application" y asignarle el valor de TRUE a la variable de " $config['composer_autoload'] ".

        Si se sigue esta opción, el cursor de la terminal deberá estar situado dentro de la carpeta "./application" antes de gestionar cualquier cosa relativa a las dependencias de Composer.


**************************************
Motor de Plantillas en CodeIgniter 3.x
**************************************

No obstante, en la propia documentación, se advierte que el uso excesivo de esta técnica puede afectar al rendimiento o carga de la página con respecto al mejor funcionamiento si se realiza todo con PHP puro.

    -> Template Parser Class - Documentación:
        https://codeigniter.com/userguide3/libraries/parser.html

Un video que explica cómo aplicar esta clase, dando algún ejemplo:

    -> 23. Librería Parser - Curso de CodeIgniter
        https://www.youtube.com/watch?v=9687xQvmRY4


Otra alternativa a esto es la posibilidad de integrar el sistema de plantillas de Blade dentro de CodeIgniter 3.x, algo que es posible instalando alguna de las dependencias de terceros que lo permite.

En esta ocasión, se emplea este paquete para disponer de Blade en el proyecto:

    -> benjamincrozat/blade (actualizado el 17/12/2020)
        https://github.com/benjamincrozat/blade

Para saber más de Blade, acceder a su `documentación <https://laravel.com/docs/8.x/blade>`_ oficial.

Aprovechando la implantación del sistema de plantillas, se efectúa una pequeña reestructuración de las carpetas de vistas para colocar, por ejemplo, cada conjunto de vistas de una sección en una carpeta propia. También, se establece una carpeta "layouts" en la que disponer de las plantillas generales de las que tirarán las vistas hijas.


***********
Migraciones
***********

Otra de las funcionalidades implantadas es la de disponer de archivos de migración relativos a las tablas de datos que deben de existir en la base de datos para dar soporte a la actividad estipulada del proyecto.

La configuración principal de las migraciones en esta versión de CodeIgniter se establece en este archivo:

    ./application/config/migration.php

De forma predeterminada, el sistema de migraciones está deshabilitado.
Para habilitarlo, basta con poner a TRUE la siguiente propiedad:

$config['migration_enabled'] = true;

Se decide seguir el tipo de versionado de Timestamp para el versionado de los archivos de migraciones.

Para sacar un código Timestamp como valor de versionado, se puede imprimir esto en una de las vistas del proyecto:

    echo date('Ymdhis');

Luego, el resultado se podrá asignar a esta propiedad del archivo de configuración, por ejemplo:

    $config['migration_version'] = 20210224180740;

Si no se establece la propiedad de "migration_type" o se obvia, el tipo predeterminado será el de "sequencial".
En este caso, si el valor de "migration_version" fuera 1, el nombre del archivo de migración a ejecutar deberá tener el prefijo de "001", por ejemplo:

    001_create_users.php


Para manejar archivos de migraciones en el proyecto, debe existir la siguiente carpeta:

    ./application/migrations


Como ya se ha mencionado, en la base de datos del proyecto, son necesarias las siguientes tablas:

    -> positions, para almacenar los puestos de trabajo.

    -> employees, para almacenar los empleados.

Debido a ello, dentro de la carpeta antes definida, se crean los correspondientes archivos de migración según la estructura de nombres para esta clase de archivos definida para su tipo de Timestamp, por ejemplo:

    20210224174028_create_positions.php

    20210224180740_create_employees.php

        NOTA EXTRA:
        Ya que la tabla de empleados (employees) dispone de un campo que la relaciona con la tabla de puestos (positions), en el sistema de migraciones a lanzar, primero, se debería crear la tabla de "positions" antes de la de "employees" para que la clave foránea implementada en su archivo de migración no diera error.

Como base de estos archivos, se coge el ejemplo de archivo de migración disponible en la documentación oficial:

    -> Migrations Class
        https://codeigniter.com/userguide3/libraries/migration.html


********************
Ejecutar Migraciones
********************

Siguiendo lo indicado en la documentación, es necesario un controlador, por ejemplo, "Migrate.php", para ejecutar las migraciones implementadas para el proyecto.

El contenido de esta clase será:

<?php

class Migrate extends CI_Controller
{
    public function index()
    {
        $this->load->library('migration');

        if ($this->migration->current() === false) {
            show_error($this->migration->error_string());
        }
    }
}

Si no se estableciera un apartado o vista particular para esta labor, de manera básica, se accederá por esta URL a la funcionalidad del controlador "Migrate":

    http://URL_DEL_SITIO/index.php/migrate

La página aparecerá en blanco (al menos, si no se produce error algún por el proceso ejecutado).

Yendo al gestor de bases de datos, dentro de la base de datos del proyecto, ya se debería disponer de las correspondientes tablas referidas a las migraciones definidas.


***********************
Estableciendo Factories
***********************

Se emplea el paquete `FakerPHP <https://fakerphp.github.io>`_ para poder generar datos aleatorios hacia la base de datos y, de esta manera, disponer de un conjunto de datos con los que practicar durante el desarrollo de la aplicación.

El archivo de clase para ejecutar una factory, por ejemplo, PositionFactory, siendo una clase propia o personalizada (no del framework o de una librería de terceros), se alojará dentro de la carpeta:

    ./application/libraries/factories/PositionFactory.php

Para cargarla dónde sea necesario:

    $this->load->library('factories/PositionFactory', '', 'positionFactory');

Al mismo tiempo, se le ha asignado un ALIAS como "positionFactory" (algo que es opcional).

Tras cargarla, para emplearla y llamar a uno de sus métodos:

    $this->positionFactory->executeFactory();


Dentro del archivo de clase de PostFactory, al no extender de una clase controlador, modelo, o vista pertenecientes al framework, no tiene la posibilidad de utilizar el método "load()", por ejemplo, para cargar un modelo de esta manera:

    $this->load->model('position_model');

Para que esto sea posible, en vez de THIS, se tendrá que emplear una instancia del objeto CodeIgniter asignado a una variable:

    $CI =& get_instance();

A partir de aquí, sustituyendo el THIS por la variable declarada, se podrá llamar al método "load()" o cualquier otro método del framework:

    $CI->load->model('position_model');
    //...
    $CI->load->helper('url');
    $CI->load->library('session');
    $CI->config->item('base_url');
    // etc.


******************
Redireccionamiento
******************

Habiendo dispuesto un VirtualHost para el proyecto, para establecer un buen funcionamiento de las redirecciones que se puedan establecer, será necesario establecer la propiedad de "base_url" con el valor del URL del VirtualHost configurado:

    $config['base_url'] = 'http://URL_ESTABLCIDA_EN_VIRTUALHOST';


****************************
Iniciando el Proyecto en NPM
****************************

A la vista de llegar a instalar alguna librería JS de terceros mediante NPM, se inicia el proyecto en este sentido para disponer de un archivo "package.json" inicial en la raíz del proyecto.

Como requerimiento previo, es necesario tener instalado NodeJS y NPM (que ya se instala con la instalación de NodeJS, al menos, en Windows).

Disponiendo, ya, de NodeJS en el sistema, se inicia el proyecto con este comando:

    |> npm init
        (para poder personalizar las respuestas demandadas)
o
    |> npm init -y
        (para establecer las respuestas predeterminadas al instante)


***************************************************
Sistema de Optimización de Archivos con Laravel Mix
***************************************************

`Laravel Mix <https://laravel.com/docs/8.x/mix>`_ es una especie de capa o envoltura (wrapper) sobre el paquete de optimización de archivos `Webpack <https://getcomposer.org>`_.

Laravel Mix proporciona una API fluida para definir los pasos de compilación de Webpack utilizando varios procesadores de CSS y JavaScript comunes.

El objetivo es, entre otras cosas, procesar todo el código CSS, minificarlo y combinarlo en un solo archivo. Lo mismo para el código JS, minificar, ofuscar y combinar todo el código en un solo archivo. Con Laravel Mix, las páginas web son más seguras y rápidas.

Laravel Mix está optimizado para usarse con Laravel, pero se puede utilizar en cualquier aplicación web, tenga Laravel o no (como en este caso).

A partir de este momento, los archivos de assets (CSS, JS, ..., incluso, los de imágenes si se desea), se establecen dentro de la carpeta "./src".

Desde esta carpeta, los archivos serán compilados hacia la carpeta "./public", según lo estipulado en el archivo "webpack.mix.js".

Los comandos de ejecución de las compilaciones de archivos se encuentran dentro del archivo "./package.json".

- Para lanzar una compilación en el entorno de desarrollo:

    |> npm run dev

- Para lanzar una compilación en el entorno de producción:

    |> npm run prod

Con este último tipo de compilación, los archivos quedarán minificados lo que añade una mejor optimización de dichos archivos.


**********************************************
Sistema de Notificaciones y Alertas Emergentes
**********************************************

Para esto, se echó mano de `SweetAlert2 <https://sweetalert2.github.io/>`_.


*****
Otros
*****

En este proyecto, se llegó a implantar una tabla de listado de empleados mediante DataTables con una configuración básica.

Para una configuración de DataTables algo más detallada, cargando los datos desde su propio AJAX, con la característica de Server Side Processing activada y la posibilidad de editar y eliminar registros desde la propia tabla sin recarga visible de la página, todo esto se encuentra disponible en el repositorio de la versión 2`versión-2 <https://bitbucket.org/PJ-CM/amie.consulting-salarios-v2/src/master/>`_ de este proyecto, repositorio en el que se ha seguido desarrollando el proyecto.

Igualmente, en el mencionado repositorio, se dispone de la generación de varias gráficas mediante el uso del paquete `Chart.js <https://www.chartjs.org>`_ .


Nada más ... Saludos.
