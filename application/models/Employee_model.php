<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Employee_model extends CI_Model
{
    private $table;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'employees';
    }

    /**
     * Insert record to table
     *
     * @param array $data
     * @return int
     */
    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    /**
     * Delete all records from table
     *
     * @return int row affected
     */
    public function deleteAll()
    {
        return $this->db->empty_table($this->table);
    }
}
