<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Position_model extends CI_Model
{
    private $table;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'positions';
    }

    /**
     * Insert record to table
     *
     * @param array $data
     * @return int
     */
    public function insert($data)
    {
        return $this->db->insert($this->table, $data);
    }

    /**
     * Get all records from table
     *
     * @return array
     */
    public function getAll()
    {
        $this->db->select('*');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    /**
     * Delete all records from table
     *
     * @return int row affected
     */
    public function deleteAll()
    {
        return $this->db->empty_table($this->table);
    }
}
