<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Salarios_model extends CI_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table_employees = 'employees';
        $this->table_positions = 'positions';
    }

    public function getEmployeesPositions()
    {
        $this->db->select('table_1.*, table_2.description');
        $this->db->from($this->table_employees . ' AS table_1');
        $this->db->join($this->table_positions . ' AS table_2', 'table_1.position_id = table_2.id');
        $this->db->order_by('table_1.id');
        $query = $this->db->get();
        return $query->result();
    }
}
