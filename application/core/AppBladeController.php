<?php
defined('BASEPATH') or exit('No direct script access allowed');

use BC\Blade\Blade;

class AppBladeController extends CI_Controller
{
    protected $views = APPPATH . 'views';
    protected $cache = APPPATH . 'cache';
    protected $blade;

    public function __construct()
    {
        parent::__construct();
        $this->blade = new Blade($this->views, $this->cache);
    }

    /**
     * Rendering the passed view with optional data
     * - If the view is on first level of view's folder
     * like "/views/nameview.blade.php", just its received
     * the "nameview"
     * - If the view is inside a views's subfolder, its received
     * the "path/nameview" or "path.nameview"
     *
     * @param [type] $view
     * @param array $data
     * @return void
     */
    public function view($view, $data = [])
    {
        echo $this->blade->make($view, $data);
    }
}
