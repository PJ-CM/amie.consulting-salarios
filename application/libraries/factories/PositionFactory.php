<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PositionFactory
{
    protected $model;
    private $CI;

    protected $_arr_positions = [
        'Consultor@ de Dirección', 'Administrador@', 'Diseñador@ UI',
        'Ingenier@ de Software', 'Comercial',
    ];

    public function __construct()
    {
        $this->CI =&get_instance();
        $this->CI->load->model('position_model');
    }

    public function executeFactory()
    {
        // Vaciando la tabla antes de volver a insertar registros ficticios
        $this->CI->position_model->deleteAll();

        foreach ($this->_arr_positions as $row) {
            $description = $row;

            $data = [
                'description' => $description,
            ];

            // Hacer un INSERT en la tabla por cada ciclo pasando
            // cada dato predeterminado
            $this->CI->position_model->insert($data);
        }
    }
}
