<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Faker\Factory as FakerFactory;

class EmployeeFactory
{
    protected $model;
    protected $faker;
    private $CI;

    protected $_arr_employees = [
        [
            'name'             => 'José Manuel Jáuregui',
            'age'              => 55,
            'registrationDate' => '2007-06-04 13:38:12',
            'position_id'      => 1,
            'salary'           => 5000,
        ],
        [
            'name'             => 'Annie Narváez',
            'age'              => 30,
            'registrationDate' => '2015-03-22 09:00:48',
            'position_id'      => 1,
            'salary'           => 3500,
        ],
        [
            'name'             => 'Alba Zabala',
            'age'              => 45,
            'registrationDate' => '2011-01-28 11:00:28',
            'position_id'      => 2,
            'salary'           => 3000,
        ],
        [
            'name'             => 'Diego Lorenzo',
            'age'              => 30,
            'registrationDate' => '2017-09-04 08:00:42',
            'position_id'      => 4,
            'salary'           => 1500,
        ],
        [
            'name'             => 'Eneko Ruiz de Loizaga',
            'age'              => 25,
            'registrationDate' => '2019-05-15 09:50:00',
            'position_id'      => 4,
            'salary'           => 1200,
        ],
    ];

    public function __construct()
    {
        $this->CI =&get_instance();
        $this->CI->load->model('position_model');
        $this->CI->load->model('employee_model');
        $this->faker = FakerFactory::create($this->CI->config->item('faker_locale'));
    }

    public function getRandomPositionId()
    {
        $data = $this->CI->position_model->getAll();
        return $data[array_rand($data)]->id;
    }

    public function executeFactory($totalRows = 22)
    {
        // Vaciando la tabla antes de volver a insertar registros ficticios

        $this->CI->employee_model->deleteAll();

        foreach ($this->_arr_employees as $row) {
            $this->CI->employee_model->insert($row);
        }

        foreach (range(1, $totalRows) as $row) {
            // Día Aleatoria entre dos fechas en Timestamp
            // formateado a Datetime
            $randomDate = $this->faker->dateTimeBetween($startDate = '-14 years', $endDate = 'now')->format($this->CI->config->item('log_date_format'));

            /**/
            $name             = $this->faker->name();
            $age              = $this->faker->numberBetween($min = 18, $max = 69);
            $registrationDate = $randomDate;
            $position_id      = $this->getRandomPositionId();
            $salary           = $this->faker->numberBetween(1000, 1600);

            $data = [
                'name'             => $name,
                'age'              => $age,
                'registrationDate' => $registrationDate,
                'position_id'      => $position_id,
                'salary'           => $salary,
            ];

            // Hacer un INSERT en la tabla por cada ciclo pasando los datos ficticios
            $this->CI->employee_model->insert($data);
        }
    }
}
