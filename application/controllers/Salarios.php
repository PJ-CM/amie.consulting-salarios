<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Salarios extends AppBladeController
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->load->model('Salarios_model');

        // $empleados = $this->Salarios_model->obtenerEmpleados();
        // $datos = array(
        // 	'empleados' => $empleados
        // );
        // $this->load->view('salarios_view', $datos);

        $records = $this->Salarios_model->getEmployeesPositions();
        $data    = [
            'records' => $records,
        ];

        // $this->load->view('salarios_view');
        $this->view('salaries.index', $data);
    }
}
