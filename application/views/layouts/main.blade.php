<?php
defined('BASEPATH') or exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> @yield('title') </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <style type="text/css">

        /* Tu propio estilo aquí */

    </style>

    <link rel="stylesheet" href="css/app.css">
</head>
<body>

    @include('layouts.main-navbar')
    <div class="container-fluid">
        @yield('content')
    </div>

    <footer>
        <div class="copyright">
            &copy; {{ date('Y') }} &bull; Amie Consulting
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <script type="text/javascript">

        // Tu javascript aquí

    </script>
    {{-- <script src="js/app.js"></script> --}}
    {{-- Opción 1d2 --}}
    {{-- @yield('footer_scripts_content') --}}
    {{-- Opción 2d21 --}}
    @stack('scripts')
</body>
</html>
