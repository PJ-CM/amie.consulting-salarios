    {{-- <nav class="navbar navbar-expand-lg navbar-light bg-darky d-flex justify-content-around">
        <a class="navbar-brand" href="/" title="Amie Consulting S.L.">
            <img src="images/logo-amie-web-2d2.png" class="d-inline-block align-top" alt="Amie Consulting S.L.">
        </a>
        <div class="collapse navbar-collapse navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin">Admin</a>
                </li>
            </ul>
        </div>
    </nav> --}}


    <nav>
        <a href="/" class="logo" title="Amie Consulting S.L.">
            <img src="images/logo-amie-web.png" class="d-inline-block align-top" alt="Amie Consulting S.L.">
        </a>
        <ul>
            <li><a href="/"@if (uri_string() == '') class="active"@endif>Home</a></li>
            <li><a href="admin"@if (uri_string() == 'admin') class="active"@endif>[Admin]</a></li>
        </ul>
    </nav>
