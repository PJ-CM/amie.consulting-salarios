@extends('layouts.main')

@section('title', 'Salarios - Tabla')

@section('content')
    <div class="card">
        <div class="text-center card-header">
            <h3 class="card-title">Salarios de los Empleados</h3>
        </div>
<?php
            if(count($records) > 0): ?>
        <div class="legend">
            <div>
                <span>Emplead@s con salario > 1500 €</span>
                <div class="legend-square bg-warning"></div>
            </div>
            <div>
                <span>Emplead@s más antiguos (+10 años)</span>
                <div class="legend-square bg-info"></div>
            </div>
        </div>
<?php       endif; ?>
        <div class="card-body" style="min-height: 600px !important;">
            <!-- Cuerpo del CARD -->
            <?php
                //print_r($records);
            ?>
            <table class="table table-hover">
<?php
            if(count($records) == 0): ?>
                <tbody>
                    <tr>
                        <td class="p-5 text-center">No hay registros en estos momentos.</td>
                    </tr>
                </tbody>
<?php
            else: ?>
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Fecha Alta</th>
                        <th scope="col">Puesto</th>
                        <th scope="col">Salario</th>
                    </tr>
                </thead>
                <tbody>
<?php
                foreach($records as $record):
                    //$registrationYear = date('Y', strtotime($record->registrationDate));
                    $currentYear =  date("Y");
                    /*echo '<br>' . $currentYear . ' - ' . date('Y', strtotime($record->registrationDate));
                    echo '<br>' . ($currentYear - date('Y', strtotime($record->registrationDate)));*/
                    //echo '<br>' . $registrationYear ' - ' $currentYear;

                    $isHigherRegistrationDate = ($currentYear - date('Y', strtotime($record->registrationDate))) > 10 ? true : false;
                ?>
                    <tr @if ($record->salary > 1500) class="bg-warning" @endif>
                        <th scope="row"><?= $record->id ?></th>
                        <td @if ($isHigherRegistrationDate) class="text-white bg-info" @endif><?= $record->name ?></td>
                        {{-- <td><?= $record->name ?></td> --}}
                        <td><?= $record->age ?></td>
                        <td><?= date('d/m/Y', strtotime($record->registrationDate)) ?></td>
                        <td><?= $record->description ?></td>
                        <td><?= $record->salary ?> €</td>
                    </tr>
<?php           endforeach; ?>
                </tbody>
<?php       endif; ?>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="js/app.js"></script>
@endpush
