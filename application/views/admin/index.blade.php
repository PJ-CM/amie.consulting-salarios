@extends('layouts.main')

@section('title', 'ADMIN')

@section('content')
    <div class="card">
        <div class="card-header text-center">
            <h3 class="card-title">ADMIN</h3>
        </div>
        <div class="card-body" style="min-height: 600px !important;">
            <!-- Cuerpo del CARD -->

<?php //if (isset($_SESSION['processReturn']) && $_SESSION['processReturn']['msg'] !== ''): ?>
<?php
    //$msg = $_SESSION['processReturn']['msg'];
    //// unset($_SESSION['processReturn']);
?>
  {{-- <div class="notification success">
    <?= //$msg ?>
  </div> --}}
<?php //endif; ?>

            <div class="dashboard-panel">
                <h4>Ejecutar Migraciones</h4>
                <div>
                    <div><a href="/admin/db/migrate" title="Ejecutar las migraciones">Ejecutar <strong>Proceso</strong></a></div>
                </div>
            </div>

            <div class="dashboard-panel">
                <h4>Ejecutar Factorías</h4>
                <div>
                    <div><a href="/admin/db/factories/position" title="Generar FACTORY correspondiente">Para el modelo <strong>Position</strong></a></div>
                    <div><a href="/admin/db/factories/employee" title="Generar FACTORY correspondiente">Para el modelo <strong>Employee</strong></a></div>
                    <div><a href="#" title="Generar FACTORY correspondiente">Para el modelo <strong>Xxx3</strong></a></div>
                    <div><a href="#" title="Generar FACTORY correspondiente">Para el modelo <strong>Xxx4</strong></a></div>
                    <div><a href="#" title="Generar FACTORY correspondiente">Para el modelo <strong>Xxx5</strong></a></div>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('scripts')
    <script src="js/app.js"></script>

<?php
if (isset($_SESSION['processReturn']) && $_SESSION['processReturn']['msg'] !== ''):
    $type = $_SESSION['processReturn']['result'];
    $message = $_SESSION['processReturn']['msg'];
    unset($_SESSION['processReturn']);
?>
    <script type="text/javascript">
        // SwalNotification.small('{{ $type }}', '{{ $message }}')
        SwalNotification.small(
            <?= json_encode($type) ?>,
            <?= json_encode($message) ?>
        );
    </script>
<?php
endif; ?>
@endpush
