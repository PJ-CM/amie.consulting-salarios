import { SwalNotification } from "./SwalNotification";

document.addEventListener("DOMContentLoaded", domReady());

function domReady() {
  changingNavbarOnScrolling();
}

function changingNavbarOnScrolling() {
  window.addEventListener("scroll", function () {
    let nav = document.querySelector("nav");
    let imgLogo = document
      .getElementsByClassName("logo")[0]
      .getElementsByTagName("img")[0];

    nav.classList.toggle("sticky", window.scrollY > 0);

    if (window.scrollY > 0) {
      imgLogo.src = "images/logo-amie-web-2d2.png";
    } else {
      imgLogo.src = "images/logo-amie-web.png";
    }
  });
}

// Registro global de variable/función
// ----------------------------------------------------------

window.SwalNotification = new SwalNotification();
